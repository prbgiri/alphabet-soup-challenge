# Alphabet Soup Challenge

## Requirements
<uL>
    <li>Within the grid of characters, the words may appear vertical, horizontal or diagonal.</li>
    <li>Within the grid of characters, the words may appear forwards or backwards.</li>
    <li>Words that have spaces in them will not include spaces when hidden in the grid of characters.</li>
</ul>

## Assumptions
<ul>
    <li>The grid characters populated loading the input file is case senstive i.e. grid matches what the input file provides</li>
    <li>Space is used between characters in the input file</li>
</ul>

## Build Environment
<ul>
    <li>Maven build tool is used with JAVA 8 SDK (should work with latest SDK as well) to compile and package a jar artifact.</li>
    <li>JUnit used for unit testing</li>
</ul>

## How-Tos
### Combined
`mvn clean package`

### Clean and Package (Skip Tests)
`mvn clean package -DskipTests=True`

### Testing
`mvn test`

### Execute Program
The program requires an input file to be passed in <br/>
`java -jar target/alphabetsoup-1.0.0-SNAPSHOT.jar <input_file>`

Example:<br />
`java -jar target/alphabetsoup-1.0.0-SNAPSHOT.jar src/test/resources/test1.txt`
