package com.alphabetsoup;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Files;

import java.util.List;
import java.util.ArrayList;
import java.util.stream.IntStream;

import java.io.IOException;

/**
 * Description: Starting point of the program that reads in a user provided file with characters grid and word(s) to search for.
 * If the target word is found in the grid, It prints out the start and end node coordinates in the grid. 
 * Else it prints Not found and exists.
 *
 * @date  09/22/2021
 * @author PGiri
 */
public class Main {

    // list to hold the words to be searched
    private final ArrayList<String> words;

    // constructor
    public Main() {
        words = new ArrayList<>();
    }


    /**
     * Description: Method to read in the content from the filepath passed in as parameter
     * and generate an multi dimensional array with the file content as puzzle characters
     * @param filePath String: Path to the file to be read in
     * @return Returns mxn Node array that holds the puzzle characters
     */
    public Node[][] readFile(String filePath) {
        // Node array to hold the puzzle characters
        Node[][] puzzle = null;

        try {
            File file = new File(filePath);
            List<String> content = Files.readAllLines(file.toPath());

            // get the array size
            String[] size = content.get(0).split("x");
            int rows = Integer.parseInt(size[0]);
            int cols = Integer.parseInt(size[1]);

            puzzle = new Node[rows][cols];

            // load the puzzle array with the characters
            for( int row = 0; row < rows; row++ ) {
                String item = content.get(row+1);
                String[] items = item.split(" ");
                for( int col = 0; col < cols; col++ ) {
                    puzzle[row][col] = new Node(row, col, items[col].charAt(0));    
                } 
            }

            // load the target words to be searched
            IntStream.range(rows+1, content.size()).forEach(row -> words.add(content.get(row)));

        } catch( IOException ex ) { 
            System.err.println("There was an issue reading the content of the file. " + 
                "Please make sure the file exists and it meets the input file requirements.");
        } finally {
            return puzzle;
        }
    }

    /**
     * Description: Getter to return the words to be searched
     * @return Returns ArrayList with target words
     */
    public ArrayList<String> getWords() {
        return this.words;
    }


    /**
     * Description: Starting point of the program
     * @param args String[]: User provided parameters
     */
    public static void main(String[] args) {
        // ensure the user provided the input file to populate the grid
        if( args.length == 0 ) {
           System.err.println("Please provide an input file"); 
        } else {
            Main main = new Main();
            
            // read the file content    
            Node[][] puzzle = main.readFile(args[0]);
             
            if( puzzle != null && puzzle.length > 0 && puzzle[0].length > 0 ) {
                // start the game 
                Puzzle puzzleGame = new Puzzle(puzzle);  
                main.getWords().forEach(word -> puzzleGame.findWord(word));
            }
        }
    }
}
