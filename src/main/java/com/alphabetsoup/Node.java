package com.alphabetsoup;

/**
 * Description: Entity to hold the coordinate of the character from the grid that are passed in as parameters.
 * @date:  09/22/2021
 * @author PGiri
*/

public class Node {
    private final int x;
    private final int y;
    private final char character;

    // constructor
    public Node(int x, int y, char character) {
        this.x          = x;
        this.y          = y;
        this.character  = character;
    }

    // getters
    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public char getChar() {
        return this.character;
    }

    @Override
    public String toString() {
        return " " + ""+x + ":" + ""+y;
    }
}
