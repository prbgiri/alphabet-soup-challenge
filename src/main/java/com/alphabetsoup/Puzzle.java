package com.alphabetsoup;

import java.util.List;
import java.util.Arrays;
import java.util.Optional;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Collections;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.stream.IntStream;
import java.util.stream.Collectors;


/**
 * Description: Class that simulates the puzzle word search game.
 * The problem can be solved leveraging a backtracking algorithm that utilizes Depth First Search 
 * to find the path and backtrack to the previous state when it does not find the character in the path.
 *
 * @date  09/22/2021
 * @author PGiri
 */
public class Puzzle {

    // Grid to hold the characters of the puzzle
    private final Node[][] puzzle;

    // constructor
    public Puzzle(Node[][] puzzle) {
        this.puzzle = puzzle;
    }

    /**
     * Description: Method to search for the target parameter in the grid.
     * @param target String: Target word.
     * @param returns true if found. False otherwise.
     */
    public boolean findWord(String target) {
        int rows = puzzle.length;
        int cols = puzzle[0].length;

        // list to collect the visited nodes
        ArrayList<Node> visited = new ArrayList<>();

        if( puzzle != null && puzzle.length > 0 && target != null && !target.isEmpty() ) {
            // variable to track the current character search within the target word
            int charIdx = 0;

            // iterate over each coordinate in the grid and see if there exists a path
            IntStream.range(0, rows).forEach(row -> {
                IntStream.range(0, cols).forEach(col -> {
                    hasPath(rows, cols, row, col, target, charIdx, visited);
                });
            });
        }

        // print out the output and return
        List<Node> matched = compare(visited, target);
        if( matched != null && matched.size() > 0 ) {
            Node startNode = matched.get(0);
            Node endNode   = matched.get(matched.size()-1);
            System.out.println(getChars(matched) + startNode + endNode);
            return true;
        }
            
        System.err.println(target + " Not found in the puzzle");
        return false;
    }

    /**
     * Description: Recursive method that utilizes depth first search approach to the find the path 
     * from the current character to the next. It looks horizontally, vertically and diagonally 
     * to search for the path and adds to the visited node for further processing.
     *
     * @param  rows   int:    Total rows in the grid
     * @param  cols   int:    Total columns in the grid
     * @param  row    int:    Current row being assessed
     * @param  col    int:    Current column being assessed
     * @param  target String: Word being searched
     * @param  visited Arraylist: Nodes that have been visited to far
     * @return true if path found. False otherwise.
     */
    private boolean hasPath(
            int rows, int cols, 
            int row, int col, 
            String target, int charIdx,
            ArrayList<Node> visited) { 

        if( charIdx >= target.length() ) {
            return true;
        }

        boolean foundAPath = false;
        boolean foundADiagPath = false;

        // ensure the search is not going out of bounds
        if( row >= 0 && row < rows && col >= 0 && col < cols ) {
             
            Node node = puzzle[row][col];

            // ensure the character at the particular row and col match the character of the target at the charIdx
            if( node.getChar() == target.charAt(charIdx) ) {

                charIdx += 1;
                visited.add(node);

                // search in all horizontal and vertical directions
                foundAPath = hasPath(rows, cols, row,   col-1, target, charIdx, visited) ||
                             hasPath(rows, cols, row-1, col,   target, charIdx, visited) ||
                             hasPath(rows, cols, row,   col+1, target, charIdx, visited) ||
                             hasPath(rows, cols, row+1, col,   target, charIdx, visited);

                // search diagonally
                foundADiagPath = hasPath(rows, cols, row-1, col-1, target, charIdx, visited) ||
                                 hasPath(rows, cols, row+1, col+1, target, charIdx, visited) ||
                                 hasPath(rows, cols, row-1, col+1, target, charIdx, visited) ||
                                 hasPath(rows, cols, row+1, col-1, target, charIdx, visited);

                charIdx -= 1;
            }
        }

        return  foundAPath || foundADiagPath;
    }

    /**
     * Description: Method to check if the passed in nodes are all in diagonal.
     * @param  nodes List: List of nodes to be checked
     * @return Return true if all the nodes are in diagonal coordinates. False otherwise.
     */
    public boolean isDiagonal(List<Node> nodes) {
        boolean allValidX = false;
        boolean allValidY = false;

        // sort the x's 
        List<Node> x = nodes.stream().sorted(Comparator.comparingInt(Node::getX)).collect(Collectors.toList());

        // to be diagonal, the x coordinates have to be in increasing order by at most 1
        allValidX = IntStream.range(0, x.size()-1).boxed().allMatch(idx -> x.get(idx).getX() < x.get(idx+1).getX());

        if( allValidX ) {
            // to be diagonal, the y coordinates also have to be in increasing order by at most 1
            List<Node> y = nodes.stream().sorted(Comparator.comparingInt(Node::getY)).collect(Collectors.toList());
            allValidY = IntStream.range(0, y.size()-1).boxed().allMatch(idx -> y.get(idx).getY() < y.get(idx+1).getY());
        }

        return allValidX && allValidY;
    }

    /**
     * Description: Method to check if the parameter nodes are either horizontally or vertically in sequence (not both).
     * @param  node List: List of nodes to be checked
     * @return Return true if all the nodes are either horizontal or vertically aligned. False otherwise.
     */
    public boolean isHorizOrVert(List<Node> nodes) {
        Node firstNode = nodes.get(0);

        // check horizontally
        boolean isHoriz = IntStream.range(1, nodes.size()).allMatch(idx -> nodes.get(idx).getX() == firstNode.getX());

        // check vertically
        boolean isVert = IntStream.range(1, nodes.size()).allMatch(idx -> nodes.get(idx).getY() == firstNode.getY());

        return isHoriz || isVert;
    }
    
    /*
     * Description: Method to get the string composed of the parameter nodes
     * @param  nodes ArrayList: Nodes to get each characters from 
     * @return String: String formed with no spaces
     */
    private String getChars(List<Node> nodes) {
        StringBuilder builder = new StringBuilder();
        nodes.forEach(node -> builder.append(node.getChar()));
        return builder.toString();
    }

    /*
     * Description: Method to find the perfect match within the visited nodes.
     * It ensures that the horizontal, vertical and diagonal requirements are met.
     * @param  visited ArrayList: Nodes visited during the search
     * @param  target String:     Target word that was searched in the grid
     * @return Return list of perfect match nodes within the grid
     */
    private List<Node> compare(ArrayList<Node> visited, String target) {
        // get the matching indices of the found nodes
        String characters = getChars(visited); 

        Pattern pattern = Pattern.compile(target);
        Matcher matcher = pattern.matcher(characters);
        List<Node> matched = null;

        while( matcher.find() ) {
            int start = matcher.start();
            int end = matcher.end();

            matched = visited.subList(start, end);

            // check if the requirements are met i.e. horizontally, vertically or diagonally
            if( !isDiagonal(matched) && !isHorizOrVert(matched) ) {
                // reverse and see if there is a match    
                Collections.reverse(matched);

                if( !isDiagonal(matched) && !isHorizOrVert(matched) ) {
                    // the word does not exist so set it to null and move on
                    matched = null;
                }
            }

            // return the perfect match, if any
            if( matched != null ) {
                return matched;
            }
        }

        return matched; 
    }
}

