package com.alphabetsoup;

import java.util.List;
import java.util.ArrayList;

import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import java.nio.file.Paths;

public class TestPuzzle {

    private final Main main;

    public TestPuzzle() {
        main = new Main();
    }

    @Test
    public void testIsDiagonal() {
        List<Node> nodes = new ArrayList<>();
        nodes.add(new Node(0, 0, 'A'));
        nodes.add(new Node(1, 1, 'B'));
        nodes.add(new Node(2, 2, 'C'));

        Puzzle puzzle = new Puzzle(null);

        assertTrue(puzzle.isDiagonal(nodes));

        nodes.add(2, new Node(2, 1, 'C'));
        assertFalse(puzzle.isDiagonal(nodes));
    }

    @Test
    public void testIsHoriz() {
        List<Node> nodes = new ArrayList<>();
        nodes.add(new Node(0, 0, 'A'));
        nodes.add(new Node(0, 1, 'B'));
        nodes.add(new Node(0, 2, 'C'));

        Puzzle puzzle = new Puzzle(null);

        assertTrue(puzzle.isHorizOrVert(nodes));

        nodes.add(2, new Node(1, 1, 'C'));
        assertFalse(puzzle.isHorizOrVert(nodes));
    }

    @Test
    public void testIsVert() {
        List<Node> nodes = new ArrayList<>();
        nodes.add(new Node(0, 0, 'A'));
        nodes.add(new Node(1, 0, 'B'));
        nodes.add(new Node(2, 0, 'C'));

        Puzzle puzzle = new Puzzle(null);

        assertTrue(puzzle.isHorizOrVert(nodes));

        nodes.add(2, new Node(1, 1, 'C'));
        assertFalse(puzzle.isHorizOrVert(nodes));
    }

    private void executeFindWords(String testFilePath) {

        // read in the test file
        Puzzle game = new Puzzle(main.readFile(testFilePath));

        // run the search words
        main.getWords().forEach(word -> {
            assertTrue(game.findWord(word));
        });
    }

    @Test
    public void testPuzzle1() {
        // read in the test file
        String testFilePath = Paths.get("src", "test", "resources", "test1.txt").toFile().getAbsolutePath();

        Puzzle game = new Puzzle(main.readFile(testFilePath));

        // run the search words
        main.getWords().forEach(word -> { assertTrue(game.findWord(word));
        });

        // run some more cases
        assertTrue(  game.findWord("DEF"));
        assertTrue(  game.findWord("CFI"));
        assertTrue(  game.findWord("HEB"));
        assertTrue(  game.findWord("GEC"));
        assertTrue(  game.findWord("IEA"));
        assertTrue(  game.findWord("I"));

        assertFalse( game.findWord("AEH"));
        assertFalse( game.findWord("AEG"));
        assertFalse( game.findWord("BED"));
        assertFalse( game.findWord("DEB"));
        assertFalse( game.findWord("BEF"));
        assertFalse( game.findWord("ABCF"));
    }

    @Test
    public void testPuzzle2() {
        String testFilePath = Paths.get("src", "test", "resources", "test2.txt").toFile().getAbsolutePath();
        executeFindWords(testFilePath); 
    }

    @Test
    public void testPuzzle3() {
        String testFilePath = Paths.get("src", "test", "resources", "test3.txt").toFile().getAbsolutePath();
        executeFindWords(testFilePath); 
    }

}
